/*

###################
#                 #
#  ASSET OPTIONS  #
#                 #
###################

TYPE -------- OPTION -------- DEFAULT ------ DESCRIPTION
global ------ path ---------- undefined ---- pattern to match file in input folder
css --------- outputs ------- compressed --- output style (compressed, expanded, etc)
css --------- autoprefix ---- true --------- automatically adds vendor prefixes for unsupported css (e.g. -webkit, -moz)
css --------- precision ----- 8 -------------number precision - useful if compiling bootstrap with its line-height var set to 1095702915325423 decimal places
css --------- sourcemaps ---- true --------- publish sourcemap?
css --------- min_suffix ---- true --------- add .min suffix to minifed files?
css --------- show_size ----- true --------- show file size in console log (minified only)
js ---------- sourcemaps ---- true --------- publish sourcemap?
js ---------- min_suffix ---- true --------- add .min suffix to minifed files?
js ---------- show_size ----- true --------- show file size in console log (original + minified)
components -- sourcemaps ---- false -------- publish sourcemap?
components -- min_suffix ---- true --------- add .min suffix to minifed files?
components -- show_size ----- true --------- show file size in console log (original + minified)


###################
#                 #
# PUBLISH OPTIONS #
#                 #
###################

TYPE -------- OPTION -------- DEFAULT ------ DESCRIPTION
global ------ path ---------- undefined ---- pattern to publish files of type
css --------- clean --------- false -------- if true, path will be cleared before publish.
js ---------- clean --------- false -------- if true, path will be cleared before publish.
components -- clean --------- true --------- if true, path will be cleared before publish.

*/

module.exports = {

    //working directory for all sass files, bower components, etc
    assets: {
        folder:             'wp-content/themes/bornbredtalent/resources/',
    },

    //array of directories that will be published to
    publish: [
        {
            folder:         'wp-content/themes/bornbredtalent/public/',
        },
        {
            folder:         'wp-content/themes/bornbredtalent/public/',
            css: {
                clean:      true
            },
            js: {
                clean:      true
            },
            img: {
                clean:      true
            },
            components: {
                clean:      true
            }
        }
    ],

    //list all bower components, separated into arrays of packages to be concatenated
    components: {
        jquery: [ //outputs: jquery.min.js
            'wp-content/themes/bornbredtalent/resources/components/jquery/dist/jquery.js',
            'wp-content/themes/bornbredtalent/resources/components/jquery.cookie/jquery.cookie.js',
            'wp-content/themes/bornbredtalent/resources/components/jquery-placeholder/jquery.placeholder.js',
        ],
        scripts: [ //outputs: scripts.min.js
            'wp-content/themes/bornbredtalent/resources/components/fastclick/lib/fastclick.js',
        ],
        modernizr: [ //outputs: modernizer.min.js
            'wp-content/themes/bornbredtalent/resources/components/modernizr/modernizr.js'
        ],
        foundation: [ //outputs: foundation.min.js
            'wp-content/themes/bornbredtalent/resources/components/foundation/js/foundation/foundation.js',
            'wp-content/themes/bornbredtalent/resources/components/foundation/js/foundation/foundation.alert.js',
            'wp-content/themes/bornbredtalent/resources/components/foundation/js/foundation/foundation.abide.js',
            'wp-content/themes/bornbredtalent/resources/components/foundation/js/foundation/foundation.clearing.js',
            'wp-content/themes/bornbredtalent/resources/components/foundation/js/foundation/foundation.dropdown.js',
            'wp-content/themes/bornbredtalent/resources/components/foundation/js/foundation/foundation.offcanvas.js',
            'wp-content/themes/bornbredtalent/resources/components/foundation/js/foundation/foundation.reveal.js',
            'wp-content/themes/bornbredtalent/resources/components/foundation/js/foundation/foundation.slider.js',
            'wp-content/themes/bornbredtalent/resources/components/foundation/js/foundation/foundation.tab.js',
            'wp-content/themes/bornbredtalent/resources/components/foundation/js/foundation/foundation.tooltip.js',
            'wp-content/themes/bornbredtalent/resources/components/foundation/js/foundation/foundation.equalizer.js'
        ]
    }
}
