<?php
/*
    Template Name: Full Page Layout
*/
?>
<?php get_header(); ?>

<?php if( get_field('page_image') ): ?>

    <div class="full-height-image" style="background: url(<?php the_field('page_image'); ?>) center center no-repeat; background-size: cover; ">

<?php endif; ?>

<?php get_footer(); ?>


