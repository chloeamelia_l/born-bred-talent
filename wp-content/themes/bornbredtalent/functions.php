<?php

/** Function to include custom stylesheets and javascript files */
/** To add custom css - add the wp_head() to header file */
/** To add custom js - add the wp_footer() to footer file */

function bornbredtalent_script_enqueue () {
    wp_enqueue_style('bootstrapcss', get_stylesheet_directory_uri() . '/bootstrap/css/bootstrap.min.css');
    wp_enqueue_script('bootstrapjs', get_stylesheet_directory_uri() . '/bootstrap/js/bootstrap.min.js');
    wp_enqueue_style('customstyle', get_template_directory_uri() . '/css/bornbredtalent.css', array(), '1.0.0', '');
    wp_enqueue_style('customnavstyle', get_template_directory_uri() . '/css/nav.css', array(), '1.0.0', '');
    wp_enqueue_style('customliststyle', get_template_directory_uri() . '/css/talent-list.css', array(), '1.0.0', '');
    wp_enqueue_script('customjs', get_template_directory_uri() . '/js/bornbredtalent.js', array(), '1.0.0', true);
}

add_action('wp_enqueue_scripts', 'bornbredtalent_script_enqueue');


/** Add Menu option into dashboard */

function bornbredtalent_theme_setup () {
    add_theme_support('menus');

    register_nav_menu('primary' , 'Primary Header Navigation');
}

add_action('init', 'bornbredtalent_theme_setup');




