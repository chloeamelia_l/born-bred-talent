<?php
/*
    Template Name: Talent List Layout
*/
?>
<?php get_header(); ?>

<div class="container">

    <?php if (have_posts()) :

        while (have_posts()) : the_post(); ?>
            <div class="col-md-12 center-content main-padding">
                <h1> <?php the_title(); ?> </h1>
                <p> <?php the_content(); ?> </p>
            </div>
        <?php endwhile; endif; ?>
    <?php
    $args = array( 'post_type' => 'Talent');
    $loop = new WP_Query( $args );
    while ( $loop->have_posts() ) : $loop->the_post(); ?>
        <a class="" href="<?php the_permalink(); ?>">
            <div class="col-sm-3">
                <div class="page-block">
                    <div class="page-block__image" style="background-image: url(<?php the_field('talent_image'); ?>);">
                    </div>
                </div>
            </div>
        </a>
    <?php endwhile; ?>
</div>
<?php get_footer(); ?>

