<?php
/*
    Template Name: Half Page Layout
*/
?>
<?php get_header(); ?>

<?php if( get_field('page_image') ): ?>
    <div class="col-md-6">
        <img src="<?php the_field('page_image'); ?>" class="img-responsive" />
    </div>

<?php endif; ?>

<!-- Check if page has content -->

<?php if (have_posts()) :

    while (have_posts()) : the_post(); ?>


    <div class="col-md-6">
        <p> <?php the_content(); ?> </p>
    </div>
<?php endwhile; endif; ?>


<?php get_footer(); ?>
