<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <!--[if lt IE 9]>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
    <![endif]-->
    <?php wp_enqueue_script("jquery"); ?>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <div id="main">
        <div id="left"></div>
        <div id="right"></div>
        <!-- Navigation -->
        <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar top-bar"></span>
                        <span class="icon-bar middle-bar"></span>
                        <span class="icon-bar bottom-bar"></span>
                    </button>
                    <a class="logo" href="/">

                    </a>
                </div>

                <!-- Toggling -->
                <div id="navbar" class="collapse navbar-collapse">
                    <div class="navbar-inner">
                        <?php wp_nav_menu(array(
                            'menu_class' => 'nav nav-pills',
                            'container' => 'nav',
                            'fallback_cb' => false,
                            'depth' => 2,
                            'theme_location' => 'primary',
                            'echo' => true,)); ?>
                    </div>
                </div>
            </div>
        </nav>
