<?php

get_header(); ?>



        <?php

        while ( have_posts() ) : the_post();



         if( get_field('talent_image') ): ?>
            <div class="col-md-6">
                <img src="<?php the_field('talent_image'); ?>" class="img-responsive" />
            </div>

        <?php endif; ?>
            <div class="col-md-6">
                <h1> <?php the_title(); ?> </h1>
                <p> <?php the_content(); ?> </p>
            </div>
        <?php
            
            // Previous/next post navigation.
            the_post_navigation( array(
                'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'twentyfifteen' ) . '</span> ' .
                    '<span class="screen-reader-text">' . __( 'Next post:', 'twentyfifteen' ) . '</span> ' .
                    '<span class="post-title">%title</span>',
                'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'twentyfifteen' ) . '</span> ' .
                    '<span class="screen-reader-text">' . __( 'Previous post:', 'twentyfifteen' ) . '</span> ' .
                    '<span class="post-title">%title</span>',
            ) );

            // End the loop.
        endwhile;
        ?>



